# Setup Prerequisites:
- Docker (Preferably latest version)
- Java-8

**Please insall Neo4j, RabbitMQ & Zipkin in the sequencial order listed below**

# 1. Rabbit-mq setup instructions
1. clone the zipkin-rabbitmq-ops
2. <code>cd zipkin-rabbitmq-ops/rmq</code> directory

Launch the following script:
**Note**: username & password, has been set to admin by default (Ok for testing only) but, a Security-Risk for Live/Prod enviroments.
* Update the Dockerfile username & password (**Hint**: Lines 6-7).
* **Please** don't change the paswword if you intend to use this repo to execute my Spring-Cloud Projects on the fly.
- <code>vim Dockerfile</code>
- <code>./rmq-launcher.sh</code>

# 2. Zipkin Setup
1. From the project root directory <code>cd zipkin/</code>
2. Execute (in the terminal) the following to launch Zipkin:

- <code>RABBIT_URI=amqp://USER:PASSWORD@HOST-IP:5672 java -jar zipkin.jar</code>

**Note**: Note the HOST-IP/hostname must match the hostname provided in RabbitMq & also in your Spring Cloud Config Project.

# 3. Neo4j Setup
1. From the project root directory <code>cd neo4j/</code>
2. launch the Neo4j using <code>./neo4j-launcher.sh</code>

**As always please go through the scripts to have an idea of what it does.**
