docker build -f Dockerfile -t rmq-img .
docker run --name='rmq' -d -it -p 15671:15671 -p 15672:15672 -p 5672:5672 -p 25672:25672 rmq-img
docker logs rmq -f
